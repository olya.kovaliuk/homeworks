import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import {
  removeFavItem,
  addFavItem,
  updateFavItemsStorage,
} from "../../store/getFavItems/favItemsActions";
import { toggleModal } from "../../store/modal/modalActions";
import { setArticle } from "../../store/article/articleActions";

const Item = ({ name, price, url, article, color }) => {
  const { cart, favorites } = useSelector((store) => {
    return store;
  });
  const dispatch = useDispatch();

  return (
    <div className="item item__wrap">
      <img src={url} alt={name} className="item__image" />
      <h4 className="item__name">{name}</h4>
      <span className="item__price item__text">Price: {price}$</span>
      <span className="item__article item__text">Article: {article}</span>
      <span className="item__color item__text">Color: {color}</span>
      <div className="item__buttons-wrap">
        {favorites.includes(article) ? (
          <div
            className="star star__image_filled"
            onClick={() => {
              dispatch(removeFavItem(article));
              updateFavItemsStorage(favorites);
              console.log(favorites);
            }}
          ></div>
        ) : (
          <div
            className="star star__image_empty"
            onClick={() => {
              dispatch(addFavItem(article));
              updateFavItemsStorage(favorites);
              console.log(favorites);
            }}
          ></div>
        )}
        <button
          className="item__button"
          onClick={() => {
            dispatch(toggleModal());
            dispatch(setArticle(article));
          }}
        >
          {cart.includes(article) ? "Remove from cart" : "Add to cart"}
        </button>
      </div>
    </div>
  );
};

Item.propTypes = {
  cart: PropTypes.array,
  favorites: PropTypes.array,
  name: PropTypes.string,
  price: PropTypes.number,
  ulr: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
};

export default Item;
