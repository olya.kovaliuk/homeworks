import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import {
  removeCartItem,
  addCartItem,
  setCartItemsStorage,
} from "../../store/getCartItems/cartItemsActions";
import { toggleModal } from "../../store/modal/modalActions";

const updateStorageCart = (cart) => {
  localStorage.setItem("cart", JSON.stringify(cart));
  console.log(cart);
};

const Modal = () => {
  const { cart, article } = useSelector((store) => {
    return store;
  });
  const dispatch = useDispatch();
  return (
    <>
      <div
        className="modal__wrapper"
        onClick={(event) =>
          event.target.classList.contains("modal__wrapper")
            ? dispatch(toggleModal())
            : null
        }
      >
        <div className="modal">
          <button
            className={"modal__closeBtn"}
            onClick={() => {
              dispatch(toggleModal());
            }}
          >
            X
          </button>
          <h2 className={"modal__header"}>
            {cart.includes(article)
              ? "Do you really want to remove it from your cart?"
              : "Do you really want to add it to your cart?"}
          </h2>
          <div>
            <button
              className="modal__button"
              onClick={() => {
                cart.includes(article)
                  ? dispatch(removeCartItem(article))
                  : dispatch(addCartItem(article));

                dispatch(toggleModal());
                setCartItemsStorage();
                updateStorageCart(cart);
              }}
            >
              Yes
            </button>
            <button
              className="modal__button"
              onClick={() => {
                dispatch(toggleModal());
              }}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

Modal.propTypes = {
  cart: PropTypes.array,
  modal: PropTypes.bool,
  article: PropTypes.number,
};

export default Modal;
