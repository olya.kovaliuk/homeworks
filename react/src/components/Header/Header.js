import { Link } from "react-router-dom";

const Header = () => {
  return (
    <section className="header__wrap">
      <div className="header">
        <div className="header__logo"></div>
        <nav className="header__nav nav">
          <Link to="/" className="nav__link">
            Shop
          </Link>
          <Link to="/cart" className="nav__link">
            Cart
          </Link>
          <Link to="/favorites" className="nav__link">
            Favorites
          </Link>
        </nav>
      </div>
    </section>
  );
};

export default Header;
