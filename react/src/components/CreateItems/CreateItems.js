import PropTypes from "prop-types";

import Item from "../Item/Item";

const CreateItems = ({ items }) => {
  const item = items.map((item, index) => (
    <Item
      key={item.article}
      name={item.name}
      price={item.price}
      url={item.url}
      article={item.article}
      color={item.color}
    />
  ));

  return <div className="items-container">{item}</div>;
};

CreateItems.propTypes = {
  items: PropTypes.array,
};

export default CreateItems;
