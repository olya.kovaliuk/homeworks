import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Header from "../Header/Header";

import ShopPage from "../../pages/ShopPage";
import CartPage from "../../pages/CartPage";
import FavoritesPage from "../../pages/FavoritesPage";

const AppRoutes = () => {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/" exact>
          <ShopPage></ShopPage>
        </Route>
        <Route path="/cart" exact>
          <CartPage></CartPage>
        </Route>
        <Route path="/favorites" exact>
          <FavoritesPage></FavoritesPage>
        </Route>
      </Switch>
    </Router>
  );
};

export default AppRoutes;
