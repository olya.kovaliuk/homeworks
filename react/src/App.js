import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import Modal from "./components/Modal/Modal";
import AppRoutes from "./components/AppRoutes/AppRoutes";

import "./App.scss";

import { getItems } from "./store/getItems/itemsActions";
import { setFavItemsStorage } from "./store/getFavItems/favItemsActions";
import { setCartItemsStorage } from "./store/getCartItems/cartItemsActions";

const App = () => {
  const { modal } = useSelector((store) => {
    return store;
  });
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getItems());
    dispatch(setFavItemsStorage());
    dispatch(setCartItemsStorage());
  }, [dispatch]);

  return (
    <>
      <AppRoutes></AppRoutes>
      {modal && <Modal />}
    </>
  );
};

export default App;
