import PropTypes from "prop-types";
import { useSelector } from "react-redux";

import CreateItems from "../components/CreateItems/CreateItems";

const CartPage = () => {
  const { items, cart } = useSelector((store) => {
    return store;
  });

  const cartItems = [];

  cart.forEach((item, i) => {
    items.forEach((elem, i) => {
      if (elem.article === item) {
        cartItems.push(elem);
      }
    });
  });

  return (
    <>
      {cartItems.length > 0 ? (
        <CreateItems items={cartItems}></CreateItems>
      ) : (
        <p className="no-items">No items has been added</p>
      )}
    </>
  );
};

CartPage.propTypes = {
  cart: PropTypes.array,
  items: PropTypes.array,
};

export default CartPage;
