import PropTypes from "prop-types";
import { useSelector } from "react-redux";

import CreateItems from "../components/CreateItems/CreateItems";

const FavoritesPage = () => {
  const { items, favorites } = useSelector((store) => {
    return store;
  });

  const favoritesItems = [];

  favorites.forEach((item, i) => {
    items.forEach((elem, i) => {
      if (elem.article === item) {
        favoritesItems.push(elem);
      }
    });
  });

  return (
    <>
      {favoritesItems.length > 0 ? (
        <CreateItems items={favoritesItems}></CreateItems>
      ) : (
        <p className="no-items">No items has been added</p>
      )}
    </>
  );
};

FavoritesPage.propTypes = {
  favorites: PropTypes.array,
  items: PropTypes.array,
};

export default FavoritesPage;
