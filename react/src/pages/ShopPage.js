import PropTypes from "prop-types";
import { useSelector } from "react-redux";

import CreateItems from "../components/CreateItems/CreateItems";

const ShopPage = () => {
  const { items } = useSelector((store) => {
    return store;
  });

  return <CreateItems items={items}></CreateItems>;
};

ShopPage.propTypes = {
  items: PropTypes.array,
};

export default ShopPage;
