export const setFavItemsStorage = () => (dispatch) => {
  const itemsStorage = JSON.parse(localStorage.getItem("favorites"));

  if (itemsStorage) {
    dispatch(setFavItemsAction(itemsStorage));
  } else {
    localStorage.setItem("favorites", JSON.stringify([]));
    dispatch(setFavItemsAction([]));
  }
};

export const updateFavItemsStorage = (favorites) => {
  localStorage.setItem("favorites", JSON.stringify(favorites));
};

export const addFavItem = (article) => (dispatch) => {
  dispatch({
    type: "ADD_FAVORITE_ITEM",
    payload: article,
  });
};

export const removeFavItem = (article) => (dispatch) => {
  dispatch({
    type: "REMOVE_FAVORITE_ITEM",
    payload: article,
  });
};

const setFavItemsAction = (itemsStorage) => ({
  type: "SET_FAVORITES_STORAGE",
  payload: [...itemsStorage],
});
