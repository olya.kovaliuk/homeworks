import { initialStore } from "../initialStore";

export const favItemsReducer = (favItems = initialStore.favorites, action) => {
  switch (action.type) {
    case "SET_FAVORITES_STORAGE":
      return [...favItems, ...action.payload];

    case "ADD_FAVORITE_ITEM":
      return [...favItems, action.payload];

    case "REMOVE_FAVORITE_ITEM":
      return favItems.filter((el) => el !== action.payload);

    default:
      return favItems;
  }
};
