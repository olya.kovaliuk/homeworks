export const setArticle = (article) => (dispatch) => {
  dispatch({
    type: "SET_ARTICLE",
    payload: article,
  });
};
