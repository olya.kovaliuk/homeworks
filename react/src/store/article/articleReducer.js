import { initialStore } from "../initialStore";

export const articleReducer = (article = initialStore.article, action) => {
  switch (action.type) {
    case "SET_ARTICLE":
      console.log(action.payload);
      return action.payload;

    default:
      return article;
  }
};
