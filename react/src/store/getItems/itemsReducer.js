import { initialStore } from "../initialStore";

export const itemsReducer = (items = initialStore.items, action) => {
  switch (action.type) {
    case "REQUEST_ITEMS_STARTED":
      return [...items];

    case "REQUEST_ITEMS_SUCCESS":
      return [...items, ...action.payload];

    default:
      return items;
  }
};
