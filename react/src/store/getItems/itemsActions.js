import axios from "axios";

export const getItems = () => {
  return (dispatch) => {
    dispatch(getItemsStarted());
    axios
      .get("itemsList.json")
      .then((response) => {
        dispatch(getItemsSuccess(response.data));
      })
      .catch((err) => {
        console.log(err.message);
      });
  };
};

const getItemsStarted = () => ({
  type: "REQUEST_ITEMS_STARTED",
});

const getItemsSuccess = (items) => ({
  type: "REQUEST_ITEMS_SUCCESS",
  payload: [...items],
});
