import { combineReducers } from "redux";

import { itemsReducer } from "./getItems/itemsReducer";
import { favItemsReducer } from "./getFavItems/favItemsReducer";
import { cartItemsReducer } from "./getCartItems/cartItemsReducer";
import { modalReducer } from "./modal/modalReducer";
import { articleReducer } from "./article/articleReducer";

export default combineReducers({
  items: itemsReducer,
  favorites: favItemsReducer,
  cart: cartItemsReducer,
  modal: modalReducer,
  article: articleReducer,
});
