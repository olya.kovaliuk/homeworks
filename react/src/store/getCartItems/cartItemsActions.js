export const setCartItemsStorage = () => (dispatch) => {
  const itemsStorage = JSON.parse(localStorage.getItem("cart"));

  if (itemsStorage) {
    dispatch(setCartItemsAction(itemsStorage));
  } else {
    localStorage.setItem("cart", JSON.stringify([]));
    dispatch(setCartItemsAction([]));
  }
};

const setCartItemsAction = (itemsStorage) => ({
  type: "SET_CART_STORAGE",
  payload: [...itemsStorage],
});

export const addCartItem = (article) => (dispatch) => {
  dispatch({
    type: "ADD_CART_ITEM",
    payload: article,
  });
};

export const removeCartItem = (article) => (dispatch) => {
  dispatch({
    type: "REMOVE_CART_ITEM",
    payload: article,
  });
};
