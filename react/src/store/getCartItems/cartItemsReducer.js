import { initialStore } from "../initialStore";

export const cartItemsReducer = (cartItems = initialStore.cart, action) => {
  switch (action.type) {
    case "SET_CART_STORAGE":
      return [...cartItems, ...action.payload];

    case "ADD_CART_ITEM":
      return [...cartItems, action.payload];

    case "REMOVE_CART_ITEM":
      return cartItems.filter((el) => el !== action.payload);

    default:
      return cartItems;
  }
};
