import { initialStore } from "../initialStore";

export const modalReducer = (modal = initialStore.modal, action) => {
  switch (action.type) {
    case "TOGGLE_MODAL":
      return !modal;

    default:
      return modal;
  }
};
