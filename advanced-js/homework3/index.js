class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name
  }
  set name(value) {
    return this._name = value
  }

  get age() {
    return this._age
  }
  set age(value) {
    return this._age = value
  }

  get salary() {
    return this._salary
  }
  set salary(value) {
    return this._salary = value
  }
}

let NewEmployee = new Employee('Olya', 22, 555)

console.group(['Employee'])
console.log(NewEmployee);
console.groupEnd()

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary)
    this.lang = lang;
  }

  get lang() {
    return this._lang
  }
  set lang(value) {
    return this._lang = value
  }

  get salary() {
    return (this._salary * 3)
  }
}

let NewProgrammerFirst = new Programmer('Vlad', 25, 1111, ['russian', 'ukrainian', 'english'])

console.group(['Programmer 1'])
console.log(NewProgrammerFirst);
console.log(NewProgrammerFirst.salary);
console.groupEnd()

let NewProgrammerSecond = new Programmer('Pasha', 24, 2456, ['russian', 'english'])

console.group(['Programmer 2'])
console.log(NewProgrammerSecond);
console.log(NewProgrammerSecond.salary);
console.groupEnd()

let NewProgrammerThird = new Programmer('Alla', 36, 5555, ['russian', 'ukrainian', 'english', 'french'])

console.group(['Programmer 3'])
console.log(NewProgrammerThird);
console.log(NewProgrammerThird.salary);
console.groupEnd()
