let btn = document.querySelector('.button')
btn.addEventListener('click', callAPI)

function callAPI() {
  fetch('https://api.ipify.org/?format=json')
    .then((data) => {
      return data.json()
    })
    .then((data) => {
      getAdress(`http://ip-api.com/json/${data.ip}?fields=continent,country,regionName,city,district,query`)
    })
}

function getAdress(url) {
    fetch(url)
      .then((data) => {
        return data.json()
      })
      .then((data) => {
        createHTML(data)
      })
}

function createHTML(data) {
  let container = document.querySelector('.container')
  container.insertAdjacentHTML('afterbegin',
  `<div>Continent: ${data.continent}</div>
  <div>Country: ${data.country}</div>
  <div>Region: ${data.regionName}</div>
  <div>City: ${data.city}</div>
  <div>District: ${data.district}</div>
  <div>IP: ${data.query}</div>`)
}
