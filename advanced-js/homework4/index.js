async function getFilms() {
    const responseFilms = await fetch("https://swapi.dev/api/films");
    const dataFilms = await responseFilms.json();
    const resultFilms = await dataFilms.results;

    createFilms(resultFilms)
    getCharacters(resultFilms)
}

async function getCharacters(resultFilms) {
  let resultCharacters;

  resultFilms.map(async elem => {
    const promises = elem.characters.map(async elem => {
      const response = await fetch(elem);
      return await response.json();
    })

    resultCharacters = await Promise.all(promises);
    console.log(resultCharacters);
  })

  createCharacters(resultCharacters, resultFilms)
}

function createFilms(resultFilms) {
  let container = document.querySelector(".container");

  resultFilms.forEach((item, i) => {
    let filmItem = document.createElement("div");
    filmItem.classList.add('item');
    filmItem.setAttribute("id", item.episode_id);
    filmItem.insertAdjacentHTML('afterbegin', `
    <div class='id'>Episode: ${item.episode_id}</div>
    <div class='title'>Title: ${item.title}</div>
    <div class='text'>${item.opening_crawl}</div>`);
    container.append(filmItem)
  });
}

function createCharacters(resultCharacters, resultFilms) {
  let charList = document.createElement("ul");

  resultFilms.forEach((item, i) => {
    
    resultCharacters.forEach((item, i) => {
      let charItem = document.createElement("li");
      charItem.classList.add('char-name')
      charItem.innerText = item.name;
      charList.append(charItem);
    });

    document.getElementById(item.episode_id).append(charList);
  });

}

getFilms()








// async function getItems(url) {
//     let response = await fetch(url);
//     return await response.json();
// }
//
// let films = getItems("https://swapi.dev/api/films");
//
// films
//   .then(data => {
//     let results = data.results;
//     let container = document.querySelector(".container");
//
//     results.forEach((item, i) => {
//       let filmItem = document.createElement("div");
//       filmItem.classList.add('item');
//       filmItem.setAttribute("id", item.episode_id);
//       filmItem.insertAdjacentHTML('afterbegin', `
//         <div class='id'>Episode: ${item.episode_id}</div>
//         <div class='title'>Title: ${item.title}</div>
//         <div class='text'>${item.opening_crawl}</div>`);
//       container.append(filmItem)
//     });
//
//     return results
//   })
//   .then(data => {
//     data.map(async elem => {
//       const promises = elem.characters.map(async elem => {
//         let response = await fetch(elem);
//         return await response.json();
//         })
//
//       let result = await Promise.all(promises);
//       let charList = document.createElement("ul");
//
//       result.forEach((item, i) => {
//         let charItem = document.createElement("li");
//         charItem.classList.add('char-name')
//         charItem.innerText = item.name;
//         charList.append(charItem);
//       });
//
//       document.getElementById(elem.episode_id).append(charList);
//     })
//   });
