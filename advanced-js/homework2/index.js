const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

function MissingAuthor(message) {
  this.message = message;
  this.name = 'MissingAuthor';
}
function MissingName(message) {
  this.message = message;
  this.name = 'MissingName';
}
function MissingPrice(message) {
  this.message = message;
  this.name = 'MissingPrice';
}

const filteredBooks = []

function tryFunc(item, arr) {
  if ((item.author && item.name && item.price) !== undefined) {
    filteredBooks.push(item)
  } else if (item.author === undefined) {
    throw new MissingAuthor('Missing author')
  } else if (item.name === undefined) {
    throw new MissingName('Missing name')
  } else if (item.price === undefined) {
    throw new MissingPrice('Missing price')
  }
}

function filterBooks(arr) {
  for (let item of arr) {
    try {
      tryFunc(item, books)
    } catch (e) {
      console.error(e.message);
    }
  };
}

filterBooks(books)

let root = document.querySelector('#root')
let list = document.createElement('ul');
root.append(list);

filteredBooks.forEach((item, i) => {
  for (let key in item){
      let listItem = document.createElement("li")
      listItem.innerHTML = item[key];
      list.append(listItem)
    }
})
