const array = ['value', () => 'showValue'];
const [value, showValue] = array;

console.group(['Exercise7'])
console.log(value);
console.log(showValue());
console.groupEnd()
