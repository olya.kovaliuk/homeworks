const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const newEmployee = {...employee, ...{age: 44, salary: 6666}}

console.group(['Exercise6'])
console.log(newEmployee);
console.groupEnd()
