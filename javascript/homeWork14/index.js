$(document).ready(function() {
  $(`a.section-link`).click(function() {
    $(`html, body`).animate({
      scrollTop: $($(this).attr(`href`)).offset().top + `px`
    }, {
      duration: 1000,
      easing: `swing`
    });
    return false;
  })
})

$(document).ready(function() {
  $(`a.link-home`).click(function() {
    $(`html, body`).animate({
      scrollTop: $($(this).attr(`href`)).offset().top + `px`
    }, {
      duration: 1000,
      easing: `swing`
    });
    $(`a.link-home`)[0].classList.add('display-none');
    return false;
  })
})

$(`.slide-toggle-button`).click(function() {
  $(`.popular-posts-section`).slideToggle()
  if ($(`.slide-toggle-button`).text() === 'Show') {
    $(`.slide-toggle-button`).text(`Hide`)
  } else {
    $(`.slide-toggle-button`).text(`Show`)
  }
})

window.addEventListener('scroll', function() {
  if (pageYOffset >= 500) {
    $(`a.link-home`)[0].classList.remove('display-none');
  } else {
    $(`a.link-home`)[0].classList.add('display-none');
  }
});
