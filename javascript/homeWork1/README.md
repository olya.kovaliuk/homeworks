1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
  1.1. var - оператор, объявляющий переменную. Данные внутри нее могут быть переопределнны. Данный тип переменной является глобальным, то есть, может быть вызван в любом месте кода, вне зависимости от того, где она была объявлена.
  1.2. let - оператор объявляющий переменную, данные внутри нее могут быть переопределенны.
  1.3. const - оператор объявляющий переменную, данные, которые в ней храняться не могут быть переопределенны, они постоянны.
2. Почему объявлять переменную через var считается плохим тоном?
  var - устаревшая форма переменной, ее не рекомендуют использовать так, как она создает глобальные переменные, что может привести к некоторым ошибкам работы. Приоритетными и отвечающими стандартам синтаксиса ES6 являются const и let.
