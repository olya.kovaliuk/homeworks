let inputPasswordIcon = document.querySelector('.input-password-icon');
let inputConfirmPasswordIcon = document.querySelector('.input-confirm-password-icon');
let inputPassword = document.querySelector('.input-password');
let inputConfirmPassword = document.querySelector('.input-confirm-password');
let buttonConfirm = document.querySelector('.btn');
let form = document.querySelector('.password-form');

function toggleClass(e) {
  if (e.target.previousElementSibling.type === 'password') {
    e.target.previousElementSibling.type = 'text';
    e.target.classList.add('fa-eye-slash');
    e.target.classList.remove('fa-eye');
  } else {
    e.target.previousElementSibling.type = 'password';
    e.target.classList.add('fa-eye');
    e.target.classList.remove('fa-eye-slash');
  }
}

inputPasswordIcon.addEventListener('click', toggleClass);
inputConfirmPasswordIcon.addEventListener('click', toggleClass);

form.addEventListener('submit', (e) => {
  e.preventDefault();
  if (inputPassword.value === '' && inputConfirmPassword.value === '') {
    let error = document.createElement('div');
    error.insertAdjacentText('afterbegin', 'Write passwords, please!');
    error.classList.add('error-text');
    buttonConfirm.before(error);
  } else if (inputPassword.value === inputConfirmPassword.value) {
    let errorCheck = document.querySelector('.error-text');
    if (errorCheck !== null) {
      errorCheck.remove()
    }
    alert('Welcome!');
  } else {
    let error = document.createElement('div');
    error.insertAdjacentText('afterbegin', 'Passwords do not match!');
    error.classList.add('error-text');
    buttonConfirm.before(error);
  }

  let checkErrorLenth = document.querySelectorAll('.error-text');
  if (checkErrorLenth.length === 2) {
    checkErrorLenth[1].remove()
  }
})
