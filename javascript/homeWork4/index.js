function createNewUser() {
  let firstName = prompt("What's your name?");
  let lastName = prompt("What's your last name?");
  const newUser = {
    firstName,
    lastName,
    getLogin(){
      return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
    }
  }
  console.log(`New user's full name is ${newUser.firstName} ${newUser.lastName} and login is ${newUser.getLogin()}, enjoy!`);
  return newUser;
}

const createUser = createNewUser();
createUser.getLogin();
