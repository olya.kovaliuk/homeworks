function createNewUser() {
  let firstName = prompt("What's your name?");
  let lastName = prompt("What's your last name?");
  let birthday = prompt("Write when is your birthday in format dd.mm.yyyy, please.")
  const newUser = {
    firstName,
    lastName,
    birthday,
    getLogin(){
      return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
    },
    getAge(){
      let dayBirthday = this.birthday.slice(0, 2);
      let monthBirthday = this.birthday.slice(3, 5) - 1;
      let yearBrithday = this.birthday.slice(6, 10);
      let age;

      let today = new Date();
      let todayYear = today.getFullYear();
      let todayMonth = today.getMonth();
      let todayDay = today.getDate();

      if (todayMonth > monthBirthday) {
        age = todayYear - yearBrithday;
      } else if (todayMonth === monthBirthday) {
        if (todayDay >= dayBirthday) {
          age = todayYear - yearBrithday;
        } else {
          age = todayYear - yearBrithday - 1;
        }
      } else {
        age = todayYear - yearBrithday - 1;
      }

      return age
    },
    getPassword(){
      return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
    }
  }
  console.log(`New user's full name is ${newUser.firstName} ${newUser.lastName} and login is ${newUser.getLogin()}, enjoy!`);
  console.log(`User is ${newUser.getAge()} years old`);
  console.log(`User's password is ${newUser.getPassword()}`);
  return newUser;
}

const createUser = createNewUser();
createUser.getLogin();
createUser.getAge();
createUser.getPassword();
