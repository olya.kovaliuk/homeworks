let styleLink = document.querySelector('#theme-link');
let btn = document.querySelector('.btn-change-theme');

function checkThemeOnLoad() {
  let themeOnLoad = localStorage.getItem('theme');
  if (themeOnLoad) {
    if (themeOnLoad === 'pink') {
      styleLink.href = "css/style-pink.css"
    } else if (themeOnLoad === 'normal') {
      styleLink.href = "css/style-normal.css"
    }
  }
}

function changeTheme() {
  let theme = localStorage.getItem('theme');
  if (theme !== null) {
    if (theme === 'pink') {
      styleLink.href = "css/style-normal.css"
      localStorage.setItem('theme', 'normal');
    } else if (theme === 'normal') {
      styleLink.href = "css/style-pink.css"
      localStorage.setItem('theme', 'pink')
    }
  } else if (theme === null) {
    styleLink.href = "css/style-pink.css"
    localStorage.setItem('theme', 'pink')
  }
}

window.onload = () => {
  checkThemeOnLoad()
}

btn.addEventListener('click', changeTheme);
