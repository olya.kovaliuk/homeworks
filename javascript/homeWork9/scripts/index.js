let tabs = document.querySelector('.tabs');
let tabsTitles = document.querySelectorAll('.tabs-title');
let tabsContentItems = document.querySelectorAll('.tabs-content-text');

let activeTabCheck;

tabs.addEventListener('click', (e) => {
  let activeTab = e.target;
  if (activeTab.tagName === 'LI') {
    if (activeTabCheck) {
      activeTabCheck.classList.remove('tab-title-active');
    }

    tabsTitles[0].classList.remove('tab-title-active');
    activeTabCheck = activeTab;
    activeTab.classList.add('tab-title-active');

    let tabName = e.target.innerText;

    tabsContentItems.forEach((item, i) => {
      let checkAttr = item.getAttribute('data-tab-content');
      item.classList.remove('tabs-text-active');
      if (tabName === checkAttr) {
        item.classList.add('tabs-text-active');
      }
    });
  }

})
