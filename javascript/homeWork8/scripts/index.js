let input = document.querySelector('.input-price');

input.addEventListener('focus', (event) => {
  event.target.classList.add('input-border')
});

input.addEventListener('blur', (event) => {
  event.target.classList.remove('input-border')

  let form = document.querySelector('.form-price');
  let value = input.value

  if (value) {
    let spanContainer = document.createElement('div');
    let spanPriceElement = document.createElement('span');
    let buttonElement = document.createElement('button');

    spanContainer.classList.add('span-container');
    spanPriceElement.classList.add('.span-price');
    buttonElement.classList.add('btn-reset');

    spanContainer.insertAdjacentElement('afterbegin', spanPriceElement);
    spanContainer.insertAdjacentElement('beforeend', buttonElement);
    buttonElement.insertAdjacentHTML('afterbegin', '&#10006;');

    form.prepend(spanContainer);
    spanContainer.append(spanPriceElement);
    spanContainer.append(buttonElement);

    if (value > 0) {
      spanPriceElement.insertAdjacentText('afterbegin', `Current price: ${value}`);
      spanPriceElement.classList.add('span-green-colored');
      input.classList.add('input-green-colored');
      input.classList.remove('input-red-colored');
    } else if (value <= 0) {
      spanPriceElement.insertAdjacentText('afterbegin', `Please enter correct price`);
      spanPriceElement.classList.add('span-red-colored');
      input.classList.add('input-red-colored')
      input.classList.remove('input-green-colored')
    }

    buttonElement.addEventListener('click', (e) => {
      spanContainer.remove();
      input.classList.remove('input-red-colored');
      input.classList.remove('input-green-colored');
      value = document.querySelector('.input-price').value = '';
    });

    let check = document.querySelectorAll('.span-container');
    if (check.length === 2) {
      check[1].remove()
    }
  } else {
    let spanContainer = document.querySelector('.span-container');
    if (spanContainer !== null) {
      spanContainer.remove();
      input.classList.remove('input-red-colored');
      input.classList.remove('input-green-colored');
    }
  }
});
