let images = document.querySelectorAll('.image-to-show');
let imagesWrapper = document.querySelector('.images-wrapper');
let btnContin = document.querySelector('.btn-contin');
let btnStop = document.querySelector('.btn-stop');
let buttonsWrapper = document.querySelector('.buttons-wrapper');

let index = null;
let int = null;


function play () {
  if (int !== null) {
    clearInterval(int)
  }

  function loop() {
    images.forEach((el, i) => {
      if(el.classList.contains('active-image')){
        index = i;
      }
    });
  }

  function switchClass () {
    if (index !== images.length - 1) {
      images[index].classList.remove('active-image');
      images[index + 1].classList.add('active-image');
    } else {
      images[index].classList.remove('active-image');
      images[0].classList.add('active-image');
    }
  }

  int = setInterval(() => {
    if (index !== null) {
      switchClass()
      loop()
    } else {
      loop()
      switchClass()
    }
    buttonsWrapper.classList.remove('display-none');
  }, 10000)

  btnStop.addEventListener('click', () => {
    clearInterval(int)

    btnContin.addEventListener('click', () => {
      play()
    })
  })
}


play()
