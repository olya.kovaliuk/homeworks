let burgerButton = document.querySelector('.menu-button__container');
let buttonItem = document.querySelector('.menu-button__item');
let nav = document.querySelector('.header__nav');

burgerButton.addEventListener('click', () => {
  buttonItem.classList.toggle('menu-button__item_active');
  nav.classList.toggle('header__nav_active');
})
